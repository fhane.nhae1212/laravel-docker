FROM php:7.3-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev && apt clean
RUN pecl install mcrypt-1.0.2 && docker-php-ext-enable mcrypt
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev && apt clean
RUN docker-php-ext-install -j$(nproc) iconv pdo_mysql 
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd 

WORKDIR /var/www

# Add user for laravel application
RUN groupadd -g 1000 www 
RUN useradd -u 1000 -ms /bin/bash -g www www
# Copy existing application directory contents
#COPY ./src /var/www
# Copy existing application directory permissions
#COPY --chown=www:www . /var/www
# Change current user to www
USER www
# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
